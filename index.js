
var pencolor="#000000";
var penwidth;

var isFilled = false;

var current_mode = 'Pen';

let isDrawing = false;
let isErasing = false;
let isLining = false;
let isCircling = false;
let isTriangling = false;
let isRectangling = false;
let isStaring = false;
let isHearting = false;

var has_added = false;

let x = 0;
let y = 0;

const george_canvas = document.getElementById('george_canvas');//background
const context = george_canvas.getContext('2d');

const george_canvas2 = document.getElementById('george_canvas2');//use to record current draw action and avoid flicker
const context2 = george_canvas2.getContext('2d');

var fontSize = document.getElementById('fontSize').value;
var fontFamily = document.getElementById('fontFamily').value;

george_canvas.setAttribute("id", "pen_cursor");
george_canvas2.setAttribute("id", "pen_cursor");
var mouse_cursor = "pen_cursor";


george_canvas.style.position = "absolute";
george_canvas.width = window.innerWidth*0.72;
george_canvas.height = window.innerHeight*8/9;
george_canvas.style.top = "30px";
george_canvas.style.left = "30px";

george_canvas2.style.position = "absolute";
george_canvas2.width = window.innerWidth*0.72;
george_canvas2.height = window.innerHeight*8/9;
george_canvas2.style.top = "30px";
george_canvas2.style.left = "30px";
var origin_width = george_canvas.width;
var origin_height = george_canvas.height;
//======================================Pen draw==========================================================
// event.offsetX, event.offsetY gives the (x,y) offset from the edge of the canvas.
george_canvas2.addEventListener('mousedown', event => {
  if(current_mode=='Pen' || current_mode == 'Rainbow')
  {
    context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
    x = event.offsetX;
    y = event.offsetY;
    isDrawing = true;
    myHistory.saveState(george_canvas);
  }
});

george_canvas2.addEventListener('mousemove', event => {
  if (isDrawing === true) {
      drawLine(context, x, y+35, event.offsetX, event.offsetY+35);
      x = event.offsetX;
      y = event.offsetY;
  }
  hue+=2;
  if(hue>=360) hue = 0;
  console.log("hue");
  console.log(hue);
});

window.addEventListener('mouseup', event => {
  if (isDrawing === true) {
      drawLine(context, x, y+35, event.offsetX, event.offsetY+35);
      x=0;
      y=0;
      isDrawing = false;

      var img = new Image();
      img.onload = function() {
        context.drawImage(img, 0, 0);
      }
      img.src = george_canvas2.toDataURL();
  }
});


var hue = 0;
function drawLine(context, x1, y1, x2, y2) {
    context.beginPath();
    if(current_mode=='Pen') context.strokeStyle = pencolor;
    //else if(current_mode=='Rainbow') context.strokeStyle= `hsl(${Math.floor(Math.random()*(360-0+1))+0}, 100%, 50%)`;
    else if(current_mode=='Rainbow') context.strokeStyle= `hsl(${hue}, 100%, 50%)`;
    context.lineWidth = slider.value/3 +1;
  
    context.globalCompositeOperation="source-over";

    context.moveTo(x1, y1);
    context.lineCap = "round";//make it smooth
    context.lineTo(x2, y2);
    context.stroke();
    context.closePath();
}
//=========================================Eraser====================================================
george_canvas2.addEventListener('mousedown', event => {
  if(current_mode=='Eraser')
  {
    context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
    x = event.offsetX;
    y = event.offsetY;
    isErasing = true;
    myHistory.saveState(george_canvas);
  }
});

george_canvas2.addEventListener('mousemove', event => {
  if (isErasing=== true) {
    eraseLine(context, x, y+35, event.offsetX, event.offsetY+35);
    x = event.offsetX;
    y = event.offsetY;
  }
});

window.addEventListener('mouseup', event => {
  if (isErasing === true) {
    eraseLine(context, x, y+35, event.offsetX, event.offsetY+35);
    x=0;
    y=0;
    isErasing = false;

    var img = new Image();
      img.onload = function() {
        context.drawImage(img, 0, 0);
      }
      img.src = george_canvas2.toDataURL();
    //Push();
  }
});

function eraseLine(context, x1, y1, x2, y2) {
  context.beginPath();
  context.strokeStyle = pencolor;
  context.lineWidth = slider.value/2 +4;
  context.globalCompositeOperation="destination-out";
  context.moveTo(x1, y1);
  context.lineCap = "round";//make it smooth
  context.lineTo(x2, y2);
  context.stroke();
  context.closePath();
  context.globalCompositeOperation="source-over";
}



//======================================Line draw============================================================================
george_canvas2.addEventListener('mousedown', event => {
  if(current_mode=='Line')
  {
    context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
    x = event.offsetX;
    y = event.offsetY;

    isLining = true;
    myHistory.saveState(george_canvas);
  }
 // console.log("downxy");
  //console.log(x,y);
});

george_canvas2.addEventListener('mousemove', event => {
  if (isLining === true) {
      lineLine(context2, x, y+35, event.offsetX, event.offsetY+35);
      context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
      lineLine(context2, x, y+35, event.offsetX, event.offsetY+35);
  }
  //console.log("movexy");
 // console.log(x,y);
});

window.addEventListener('mouseup', event => {
  if (isLining === true) {
      lineLine(context2, x, y+35, event.offsetX, event.offsetY+35);
      x=event.offsetX;
      y= event.offsetY;
      isLining = false;

      var img = new Image();
      img.onload = function() {
        context.drawImage(img, 0, 0);
      }
      img.src = george_canvas2.toDataURL();
  }
});

function lineLine(context, x1, y1, x2, y2) {
    context.beginPath();
    if(current_mode == 'Line') context.strokeStyle = pencolor;
    context.lineWidth = slider.value/3 +1;
  
    context.globalCompositeOperation="source-over";

    context.moveTo(x1, y1);
    context.lineCap = "round";//make it smooth
    context.lineTo(x2, y2);
    context.stroke();
    context.closePath();
}
//==============================================================================================================================

//======================================Circle draw============================================================================
george_canvas2.addEventListener('mousedown', event => {
  if(current_mode=='Circle' || current_mode=='SolidCircle')
  {
    context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
    x = event.offsetX;
    y = event.offsetY;

    isCircling = true;
    myHistory.saveState(george_canvas);
  }
});

george_canvas2.addEventListener('mousemove', event => {
  if (isCircling === true) {
      circleLine(context2, x, y+35, Math.abs(event.offsetX-x)/2 , Math.abs(event.offsetY+35-y)/2);
      context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
      circleLine(context2, x, y+35, Math.abs(event.offsetX-x)/2 , Math.abs(event.offsetY+35-y)/2);
  }
});

window.addEventListener('mouseup', event => {
  if (isCircling === true) {
      circleLine(context2, x, y+35, Math.abs(event.offsetX-x)/2 , Math.abs(event.offsetY+35-y)/2 );
      x=event.offsetX;
      y= event.offsetY;
      isCircling = false;

      var img = new Image();
      img.onload = function() {
        context.drawImage(img, 0, 0);
      }
      img.src = george_canvas2.toDataURL();
  }
});


//void ctx.ellipse(x, y, radiusX, radiusY, rotation, startAngle, endAngle, anticlockwise);
//     ctx.ellipse(100, 100, 50, 75, 45 * Math.PI/180, 0, 2 * Math.PI); //倾斜45°角
function circleLine(context, x1, y1, rx, ry) {
    context.beginPath();
    
    context.globalCompositeOperation="source-over";
    context.ellipse(x1,y1,rx,ry,0,0, 2 * Math.PI);
    
    context.closePath();
    
    if(current_mode == 'Circle') {
      context.strokeStyle = pencolor;
      context.lineWidth = slider.value/3 +1;
      
      context.lineCap = "round";//make it smooth
      context.stroke();
    }
    else if(current_mode == 'SolidCircle') {
      context.fillStyle = pencolor;
      context.fill();
    }

}
//==============================================================================================================================




/*
       x1y1


  x2y2     x3y3
*/

//======================================Triangle draw============================================================================
george_canvas2.addEventListener('mousedown', event => {
  if(current_mode==='Triangle' || current_mode=='SolidTriangle')
  {
    context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
    x = event.offsetX;
    y = event.offsetY;

    isTriangling = true;
    myHistory.saveState(george_canvas);
  }
});

george_canvas2.addEventListener('mousemove', event => {
  if (isTriangling === true) {
      triangleLine(context2, x, y+35, event.offsetX, event.offsetY+35, x-(event.offsetX-x), y+(event.offsetY+35-y));
      context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
      triangleLine(context2, x, y+35, event.offsetX, event.offsetY+35, x-(event.offsetX-x), y+(event.offsetY+35-y));
  }
});

window.addEventListener('mouseup', event => {
  if (isTriangling == true) {
      triangleLine(context2, x, y+35, event.offsetX, event.offsetY+35, x-(event.offsetX-x), y+(event.offsetY+35-y));
      x=event.offsetX;
      y= event.offsetY;
      isTriangling = false;

      var img = new Image();
      img.onload = function() {
        context.drawImage(img, 0, 0);
      }
      img.src = george_canvas2.toDataURL();
  }
});


function triangleLine(context, x1, y1, x2, y2, x3, y3) {
    context.beginPath();
    
    context.globalCompositeOperation="source-over";
    context.moveTo(x1,y1);
    context.lineTo(x2,y2);
    context.lineTo(x3,y3);

    context.closePath();

    
    if(current_mode == 'Triangle') {
      context.strokeStyle = pencolor;
      context.lineWidth = slider.value/3 +1;
      
      context.lineCap = "round";//make it smooth
      context.stroke();
    }
    else if(current_mode == 'SolidTriangle') {
      context.fillStyle = pencolor;
      context.fill();
    }

}
//==============================================================================================================================




/*
      x1y1     x2y2


      x3y3     x4y4
*/
//======================================Rectangle draw============================================================================
george_canvas2.addEventListener('mousedown', event => {
  if(current_mode=='Rectangle' || current_mode=='SolidRectangle')
  {
    context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
    x = event.offsetX;
    y = event.offsetY;

    isRectangling = true;
    myHistory.saveState(george_canvas);
  }
});

george_canvas2.addEventListener('mousemove', event => {
  if (isRectangling === true) {
      rectangleLine(context2, x, y+35, event.offsetX, event.offsetY+35, x, event.offsetY+35, event.offsetX, y+35);
      context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
      rectangleLine(context2, x, y+35, event.offsetX, event.offsetY+35, x, event.offsetY+35, event.offsetX, y+35);
  }
});

window.addEventListener('mouseup', event => {
  if (isRectangling === true) {
      rectangleLine(context2, x, y+35, event.offsetX, event.offsetY+35, x, event.offsetY+35, event.offsetX, y+35);
      x=event.offsetX;
      y= event.offsetY;
      isRectangling = false;

      var img = new Image();
      img.onload = function() {
        context.drawImage(img, 0, 0);
      }
      img.src = george_canvas2.toDataURL();
  }
});


function rectangleLine(context, x1, y1, x2, y2, x3, y3, x4, y4) {
    context.beginPath();
    
    context.globalCompositeOperation="source-over";
    context.moveTo(x1,y1);
    context.lineTo(x3,y3);
    context.lineTo(x2,y2);
    context.lineTo(x4,y4);

    context.closePath();

    
    if(current_mode == 'Rectangle') {
      context.strokeStyle = pencolor;
      context.lineWidth = slider.value/3 +1;
      
      context.lineCap = "round";//make it smooth
      context.stroke();
    }
    else if(current_mode == 'SolidRectangle') {
      context.fillStyle = pencolor;
      context.fill();
    }

}
//==============================================================================================================================

//======================================Star draw============================================================================
george_canvas2.addEventListener('mousedown', event => {
  if(current_mode=='Star' || current_mode=='SolidStar')
  {
    context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
    x = event.offsetX;
    y = event.offsetY;

    isStaring = true;
    myHistory.saveState(george_canvas);
  }
});

george_canvas2.addEventListener('mousemove', event => {
  if (isStaring === true) {
      starLine(context2, x, y+35, 5, Math.sqrt(Math.pow((event.offsetX - x), 2) + Math.pow((event.offsetY - y), 2)), Math.sqrt(Math.pow((event.offsetX - x), 2) + Math.pow((event.offsetY - y), 2))/2);
      context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
      starLine(context2, x, y+35, 5, Math.sqrt(Math.pow((event.offsetX - x), 2) + Math.pow((event.offsetY - y), 2)), Math.sqrt(Math.pow((event.offsetX - x), 2) + Math.pow((event.offsetY - y), 2))/2);
  }
});

window.addEventListener('mouseup', event => {
  if (isStaring === true) {
      starLine(context2, x, y+35, 5, Math.sqrt(Math.pow((event.offsetX - x), 2) + Math.pow((event.offsetY - y), 2)), Math.sqrt(Math.pow((event.offsetX - x), 2) + Math.pow((event.offsetY - y), 2))/2);
      x=event.offsetX;
      y= event.offsetY;
      isStaring = false;

      var img = new Image();
      img.onload = function() {
        context.drawImage(img, 0, 0);
      }
      img.src = george_canvas2.toDataURL();
  }
});


function starLine(context,cx,cy,spikes,outerRadius,innerRadius) {
    context.beginPath();
    
    context.globalCompositeOperation="source-over";
    
    var rot = Math.PI / 2 * 3;
    var localx = cx;
    var localy = cy;
    var step = Math.PI / spikes;

    context.strokeSyle = pencolor;
    context.beginPath();
    context.moveTo(cx, cy - outerRadius)
    for (i = 0; i < spikes; i++) {
        localx = cx + Math.cos(rot) * outerRadius;
        localy = cy + Math.sin(rot) * outerRadius;
        context.lineTo(localx, localy)
        rot += step

        localx = cx + Math.cos(rot) * innerRadius;
        localy = cy + Math.sin(rot) * innerRadius;
        context.lineTo(localx, localy)
        rot += step
    }

    context.closePath();
    context.lineTo(cx, cy - outerRadius);
    
    if(current_mode == 'Star') {
      context.strokeStyle = pencolor;
      context.lineWidth = slider.value/3 +1;
      
      context.lineCap = "round";//make it smooth
      context.stroke();
    }
    else if(current_mode == 'SolidStar') {
      context.fillStyle = pencolor;
      context.fill();
    }

}
//==============================================================================================================================

//======================================Heart draw============================================================================
george_canvas2.addEventListener('mousedown', event => {
  if(current_mode=='Heart' || current_mode=='SolidHeart')
  {
    context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
    x = event.offsetX;
    y = event.offsetY;

    isHearting = true;
    myHistory.saveState(george_canvas);
  }
});

var r;
george_canvas2.addEventListener('mousemove', event => {
  if (isHearting === true) {
      r = Math.sqrt(Math.pow((event.offsetX - x), 2) + Math.pow((event.offsetY - y), 2));
      heartLine(context2, x, y+35, x-r/2, y+35+r/2, x, y+35+r, x+r/2, y+35+r/2);
      context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
      heartLine(context2, x, y+35, x-r/2, y+35+r/2, x, y+35+r, x+r/2, y+35+r/2);
  }
});

window.addEventListener('mouseup', event => {
  if (isHearting === true) {
      r = Math.sqrt(Math.pow((event.offsetX - x), 2) + Math.pow((event.offsetY - y), 2));
      heartLine(context2, x, y+35, x-r/2, y+35+r/2, x, y+35+r, x+r/2, y+35+r/2);
      x=event.offsetX;
      y= event.offsetY;
      isHearting = false;

      var img = new Image();
      img.onload = function() {
        context.drawImage(img, 0, 0);
      }
      img.src = george_canvas2.toDataURL();
  }
});


function heartLine(context, x1, y1, x2, y2, x3, y3, x4, y4) {
    context.beginPath();
    
    context.globalCompositeOperation="source-over";
    context.moveTo(x1,y1);
    //context.arcTo(x2,y1,x2,y2,Math.sqrt(2)*r/4);
    context.arc((x1+x2)/2,(y1+y2)/2, Math.sqrt(2)*r/4 ,7*Math.PI/4,3*Math.PI/4,true);
    context.lineTo(x2,y2);
    context.lineTo(x3,y3);
    //context.lineTo(x4,y4);
    //context.arcTo(x4,y1,x1,y1,Math.sqrt(2)*r/4);
    context.arc((x1+x4)/2,(y1+y4)/2, Math.sqrt(2)*r/4 ,Math.PI/4,5*Math.PI/4,true);
    //context.closePath();

  
    if(current_mode == 'Heart') {
      context.strokeStyle = pencolor;
      context.lineWidth = slider.value/3 +1;
      
      context.lineCap = "round";//make it smooth
      context.stroke();
    }
    else if(current_mode == 'SolidHeart') {
      context.fillStyle = pencolor;
      context.fill();
    }

}
//==============================================================================================================================

//=====================================colorwell==========================================reference from mdn
var colorWell;
var defaultColor = "#000000";
window.addEventListener("load", startup, false);
function startup() {
    colorWell = document.querySelector("#colorWell");
    colorWell.value = defaultColor;
    colorWell.addEventListener("input", updateFirst, false);
    colorWell.select();
    console.log(pencolor);
  }
  function updateFirst(event) {
      pencolor = event.target.value;
  }
//==============================================================================

//=========================slider================================reference from w3school
var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}
//==================================================================

//=================================catch draw mode==============================================
function draw_mode(event){
  george_canvas.removeAttribute(mouse_cursor);
  george_canvas2.removeAttribute(mouse_cursor);
  switch (event) {
    case 'Pen':
      current_mode = 'Pen';
      mouse_cursor = "pen_cursor";
    break;
    case 'Line':
      current_mode = 'Line';
      mouse_cursor = 'line_cursor';
    break;
    case 'Eraser':
      current_mode = 'Eraser';
      mouse_cursor = 'eraser_cursor';
    break;
    case 'Text':
      current_mode = 'Text';
      mouse_cursor = 'text_cursor';
    break;
    case 'Circle':
      if(fill_mode) {
        current_mode = 'SolidCircle';
        mouse_cursor = 'solid_circle_cursor';
      }
      else {
        current_mode = 'Circle';
        mouse_cursor = 'circle_cursor';
      }
    break;
    case 'Triangle':
      if(fill_mode) {
        current_mode = 'SolidTriangle';
        mouse_cursor = 'solid_triangle_cursor';
      }
      else {
        current_mode = 'Triangle';
        mouse_cursor = 'triangle_cursor';
      }
    break;
    case 'Rectangle':
      if(fill_mode) {
        current_mode = 'SolidRectangle';
        mouse_cursor = 'solid_rectangle_cursor';
      }
      else {
        current_mode = 'Rectangle';
        mouse_cursor = 'rectangle_cursor';
      }
    break;
    case 'Rainbow':
      current_mode = 'Rainbow';
      mouse_cursor = 'rainbow_cursor';
    break;
    case 'Star':
      if(fill_mode) {
        current_mode = 'SolidStar';
        mouse_cursor = 'solid_star_cursor';
      }
      else {
        current_mode = 'Star';
        mouse_cursor = 'star_cursor';
      }
    break;
    case 'Heart':
      if(fill_mode) {
        current_mode = 'SolidHeart';
        mouse_cursor = 'solid_heart_cursor';
      }
      else {
        current_mode = 'Heart';
        mouse_cursor = 'heart_cursor';
      }
    break;
    default:
      current_mode = 'Pen';
      mouse_cursor = 'pen_cursor';
    break;
  }
  console.log("current_mode");
  console.log(current_mode);
  
  george_canvas.setAttribute("id",mouse_cursor);
  george_canvas2.setAttribute("id",mouse_cursor);
  console.log("mouse_cursor");
  console.log(mouse_cursor);
}
/*======================================================*/

//================================================fill
var fill_mode = false;
function fill() {
  fill_mode = !fill_mode;
  if(fill_mode) {
    document.getElementById("Circle").style.backgroundImage = "url('img/solidcircle.png')";
    document.getElementById("Triangle").style.backgroundImage = "url('img/solidtriangle.png')";
    document.getElementById("Rectangle").style.backgroundImage = "url('img/solidrectangle.png')";
    document.getElementById("Star").style.backgroundImage = "url('img/solidstar.png')";
    document.getElementById("Heart").style.backgroundImage = "url('img/solidheart.png')";
  }
  else {
    document.getElementById("Circle").style.backgroundImage = "url('img/circle.png')";
    document.getElementById("Triangle").style.backgroundImage = "url('img/triangle.png')";
    document.getElementById("Rectangle").style.backgroundImage = "url('img/rectangle.png')";
    document.getElementById("Star").style.backgroundImage = "url('img/star.png')";
    document.getElementById("Heart").style.backgroundImage = "url('img/heart.png')";
  }
}

//==========================================================

//===================refresh   clear all ================================
function Clear() {
  context.clearRect(0, 0, george_canvas.width, george_canvas.height);
  context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
  george_canvas2.width = origin_width;
  george_canvas2.height = origin_height;
  george_canvas.width = origin_width;
  george_canvas.height = origin_height;
  myHistory.undo_list = [];
  myHistory.redo_list = [];
}
//==============================================================

//=========================================================text================stackoverflow
var is_adding = false;
george_canvas2.onclick = function(event) {
  if(current_mode=='Text'){
    fontSize = document.getElementById('fontSize').value;
    fontFamily = document.getElementById('fontFamily').value;
    myHistory.saveState(george_canvas);
    context2.clearRect(0, 0, george_canvas2.width, george_canvas2.height);
    if (has_added) return;

    george_text = addInput(event.clientX, event.clientY);
 
    console.log('textonclick');
    console.log('fontSize:');
    console.log(fontSize);
    console.log('pencolor:');
    console.log(pencolor);
  }
}

var this_input;
//dynamically add input box: 
function addInput(x, y) {
  var input = document.createElement('input');

  input.type = 'text';
  input.style.position = 'fixed';
  input.style.left = (x - 4) + 'px';
  input.style.top = (y - 4) + 'px';

  this_input = input;

  is_adding = true;
  input.onkeydown = handleEnter;

  document.body.appendChild(input);

  input.focus();

  has_added = true;
}

var this_text;
var into_handleEnter = false;
//Key handler for input box:
function handleEnter(e) {
  into_handleEnter = true;
  console.log("into handleEnter!!!!");
  var keyCode = e.keyCode;
  this_text = this;
  if (keyCode === 13) {
    drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
    document.body.removeChild(this);
    has_added = false;
    into_handleEnter = false;
  }
}

//handle mouse event when input box is added
window.addEventListener('mousedown',event => {
  if(is_adding)
  {
    if(into_handleEnter) 
    {
      drawText(this_text.value,parseInt(this_text.style.left, 10),parseInt(this_text.style.top, 10));
      console.log("draw!");
      console.log("into_handleEnter:");
      console.log(into_handleEnter);
      into_handleEnter = false;
    }
    document.body.removeChild(this_input);
    
    has_added = false;
    is_adding = false;
  }  
});

//Draw the text on canvas:
function drawText(txt, x, y) {
  context.font = fontSize + "px" +" " + fontFamily;
  context.fillStyle = pencolor;
  //console.log(fontSize);
  //console.log(pencolor);
  context.globalCompositeOperation="source-over";
  context.fillText(txt, x, y);

  var img = new Image();
  img.onload = function() {
    context.drawImage(img, 0, 0);
  }
  img.src = george_canvas2.toDataURL();
}
//=====================================================================================

//========================Download============================
function Download() {
  var d = document.createElement("a");
  d.href = george_canvas.toDataURL();
  d.download = "your_canvas.png";
  d.click();
}
//=========================================================


//==========upload====================================

let upload_input = document.getElementById("input_file");
upload_input.addEventListener("change", Upload); 
function Upload(e) {
  Clear();
  var URL = window.webkitURL;
  var url = URL.createObjectURL(e.target.files[0]);

  console.log(e.target.files);
  var img = new Image();

  img.onload = function() {
    //needed to resize the canvas
    george_canvas2.width = img.width;
    george_canvas2.height = img.height;
    george_canvas.width = img.width;
    george_canvas.height = img.height;
    //
    context.drawImage(img, 0, 0);
    console.log("onload");
  }
  img.src = url;
}
//=================+++++++++++++++++++++===reference ======to record draw history==============================

var myHistory = {
  redo_list: [],
  undo_list: [],
  saveState: function(canvas, list, keep_redo) {
    keep_redo = keep_redo || false;
    if(!keep_redo) {
      this.redo_list = [];
    }
    
    (list || this.undo_list).push(canvas.toDataURL());   
  },
  undo: function(canvas, ctx) {
    this.restoreState(canvas, ctx, this.undo_list, this.redo_list);
  },
  redo: function(canvas, ctx) {
    this.restoreState(canvas, ctx, this.redo_list, this.undo_list);
  },
  restoreState: function(canvas, ctx, pop, push) {
    if(pop.length) {
      this.saveState(canvas, push, true);
      var restore_state = pop.pop();
      var img = document.createElement('img');
      img.src = restore_state;
      img.onload = function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);  
      }
    }
  }
}
//============================================================
function Undo() {
  context.globalCompositeOperation = 'source-over';
  context2.clearRect(0, 0, george_canvas.width, george_canvas.height);
  myHistory.undo(george_canvas, context);
}

function Redo() {
  context.globalCompositeOperation = 'source-over';
  myHistory.redo(george_canvas, context);
}

