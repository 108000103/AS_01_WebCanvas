# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| line                                             | 1~5%      | Y         |
| filled mode                                      | 1~5%      | Y         |
| star                                             | 1~5%      | Y         |
| heart                                            | 1~5%      | Y         |
| rainbow                                          | 1~5%      | Y         |



---

## How to use 
    

![](https://i.imgur.com/ADDH2Go.jpg)

    左邊被黑色邊框框起來的範圍是canvas可以畫圖的地方，右邊則是各種會用到的功能

---
## Function description


### Basic: Cursor icon

    當點擊功能後，滑鼠游標會在canvas區域對應當前功能顯示不同游標圖示。

### Basic: 顏色選擇器
    
    點擊右邊最上面Pen's color 旁的方框可以叫出調色盤，可以透過直接在調色盤拖曳，或可輸入RGB、HSL或是HEX，來改變畫筆的顏色。

### Basic: 畫筆大小選擇器

    在顏色選擇器的下面，透過滑動Slider來調整畫筆的粗細程度，會在Pen's Size 同時顯示當前尺寸。

### Basic: 普通畫筆 

    根據Pen' Size和Pen's Color，畫出符合所選的筆跡，利用各種mouse event不斷更新moveto及lineto實作而成。

### Other: 直線

    根據Pen' Size和Pen's Color，畫出符合所選的筆跡，與普通畫筆實作方式類似，但直線則會固定起始的xy值，
    並在上層畫布不斷draw和clear(整個canvas其實是兩個相疊，利用不同的z-index，可以避免flicker的產生)，
    使得出現軌跡皆為直線。

### Basic: 橡皮擦 

    與普通畫筆實作方式類似，但橡皮擦的global composite operation，由普通畫筆的"destination-out"改成'source-over'，
    可在經過的路徑消除canvas上做過的所有痕跡。

### Basic: Text

    可在canvas上點擊產生文字input框，輸入enter會將所輸入的文字，根據所選顏色字型大小，印上canvas，
    如果在輸入時點擊滑鼠將會直接做enter的動作，如點擊範圍剛好在canvas內，會在執行enter的同時產生一個新的文字input框。

### Other: filled 模式

    點擊filledmode icon(實心正方形on空心正方形) 會在空心和實心模式切換，預設為空心，點擊後，圓形、三角形、矩形、
    星星和愛心的UI圖示，以及點擊後對應的滑鼠游標圖示，都會由空心變為實心，反之亦然。

### Advanced: 圓形

    與直線實作方式類似，圓形利用ellipse(centerX, centerY, radiusX, radiusY)來實現畫圓圈的功能。

### Advanced: 三角形

    與直線實作方式類似，三角形利用滑鼠拖曳的起始點和結束點算出第三點位置，利用lineto來畫出三角形。

### Advanced: 矩形

    與直線實作方式類似，矩形利用鼠拖曳的起始點和結束點算出其他兩點位置，利用lineto來畫出矩形。

---
### Advanced: Undo / Redo

    當滑鼠在canvas上開始畫圖，會把當前的canvas記錄下來以圖片的形式push到undo list。

#### Undo

    點選後會回到紀錄中的上一個canvas。當點到undo的時候，如果undo list裡面有圖片，
    則把目前的canvas以圖片的形式push到redo list，同時從undo list裡面pop出圖片，
    畫在目前的canvas上，完成undo的功能。

#### Redo

    點選後會回到紀錄中的下一個canvas。當點到redo的時候，如果redo list裡面有圖片，
    則把目前的canvas以圖片的形式push到undo list，同時從redo list裡面pop出圖片，
    畫在目前的canvas上，完成redo的功能。
---

### Other: 星星

    與直線實作方式類似，星星利用滑鼠拖曳的起始點和結束點算出內半徑和外半徑，搭配cos和sin來畫出星星。

### Other: 愛心

    與直線實作方式類似，星星利用滑鼠拖曳的起始點和結束點算出參考半徑，
    搭配arc(x, y, radius, startAngle, endAngle [, counterclockwise])來畫出愛心。

### Advanced: Image tool

    點擊後可以讓使用者從本地端upload圖片到canvas，成為新的初始canvas(canvas長寬變為新圖片的長寬)。

### Advanced: Download

    點擊後可以讓使用者download當前的canvas到本地

### Basic: Refresh

    點擊後可以clear所有東西，回到初始狀態，包括canvas內容、undo redo保存的內容。

### Other: 彩虹筆

    與普通畫筆類似，彩虹筆利用在canvas拖曳滑鼠時一直改變hue的value來實現不斷切換顏色的功能。

### Basic: Font menu
    由兩個select構成，一個選字型，一個選字體大小

### Gitlab page link
    
https://108000103.gitlab.io/AS_01_WebCanvas


